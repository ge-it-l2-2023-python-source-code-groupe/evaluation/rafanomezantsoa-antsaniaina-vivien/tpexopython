from tools.console import clear
from chapitres.variables.exo_2_11 import exo_2_11_1, exo_2_11_2, exo_2_11_3
from chapitres.affichage.exo_3_6 import exo_3_6_1, exo_3_6_2, exo_3_6_3, \
    exo_3_6_4, exo_3_6_5
from chapitres.listes.exo_4_10 import exo_4_10_1, exo_4_10_2, exo_4_10_3, \
    exo_4_10_4, exo_4_10_5, exo_4_10_6
from chapitres.boucles.exo_5_4 import exo_5_4_1, exo_5_4_2, exo_5_4_3, \
    exo_5_4_4, exo_5_4_5, exo_5_4_6, exo_5_4_7, exo_5_4_8, exo_5_4_9, \
    exo_5_4_10, exo_5_4_11, exo_5_4_12, exo_5_4_13, exo_5_4_14
from chapitres.conditions.exo_6_7 import exo_6_7_1, exo_6_7_2, exo_6_7_3, \
    exo_6_7_4, exo_6_7_5, exo_6_7_6, exo_6_7_7, exo_6_7_8, exo_6_7_9_1, \
    exo_6_7_9_2, exo_6_7_9_3, exo_6_7_10
from tools.input_parsing import parse_space

while True:
    clear()
    print("Menu principal")

    print("""
Chapitre 2 Variables
    2.11.1 Nombres de Friedman - 2.11.2 Prédire le résultat : opérations -
    2.11.3 Prédire le résultat : opérations et conversions de types
Chapitre 3 Affichage
    3.6.1 Affichage dans l’interpréteur et dans un programme
    3.6.2 Poly-A - 3.6.3 Poly-A et poly-GC - 3.6.4 Écriture formatée -
    3.6.5 Écriture formatée 2
Chapitre 4 Listes
    4.10.1 Jours de la semaine - 4.10.2 Saisons -
    4.10.3 Table de multiplication par 9 - 4.10.4 Nombres pairs
    4.10.5 Liste et indice - 4.10.6 Liste et range
Chapitre 5 Boucles et comparaisons
    5.4.1 Boucles de base - 5.4.2 Boucle et jours de la semaine
    5.4.3 Nombres de 1 à 10 sur une ligne - 5.4.4 Nombres pairs et impairs
    5.4.5 Calcul de la moyenne - 5.4.6 Produit de nombres consécutifs
    5.4.7 Triangle - 5.4.8 Triangle inversé - 5.4.9 Triangle gauche
    5.4.10 Pyramide - 5.4.11 Parcours de matrice
    5.4.12 Parcours de demi-matrice sans la diagonale (exercice ++)
    5.4.13 Sauts de puce - 5.4.14 Suite de Fibonacci (exercice +++)
Chapitre 6 Tests
    6.7.1 Jours de la semaine - 6.7.2 Séquence complémentaire d’un brin d’ADN
    6.7.3 Minimum d’une liste - 6.7.4 Fréquence des acides aminés
    6.7.5 Notes et mention d’un étudiant - 6.7.6 Nombres pairs
    6.7.7 Conjecture de Syracuse (exercice +++) - 6.7.8 Attribution de la
    structure secondaire des acides aminés d’une protéine (exercice +++) -
    6.7.9 Détermination des nombres premiers inférieurs à 100 (exercice +++)
    6.7.10 Recherche d’un nombre par dichotomie (exercice +++)
""")

    option = input("Entrez le numéro de l'exercice (entrez \"q\" pour quitter)\
 : ")
    clear()
    option = parse_space(option)

    if option == "q":
        break
    elif option == "2.11.1":
        exo_2_11_1()
    elif option == "2.11.2":
        exo_2_11_2()
    elif option == "2.11.3":
        exo_2_11_3()
    elif option == "3.6.1":
        exo_3_6_1()
    elif option == "3.6.2":
        exo_3_6_2()
    elif option == "3.6.3":
        exo_3_6_3()
    elif option == "3.6.4":
        exo_3_6_4()
    elif option == "3.6.5":
        exo_3_6_5()
    elif option == "4.10.1":
        exo_4_10_1()
    elif option == "4.10.2":
        exo_4_10_2()
    elif option == "4.10.3":
        exo_4_10_3()
    elif option == "4.10.4":
        exo_4_10_4()
    elif option == "4.10.5":
        exo_4_10_5()
    elif option == "4.10.6":
        exo_4_10_6()
    elif option == "5.4.1":
        exo_5_4_1()
    elif option == "5.4.2":
        exo_5_4_2()
    elif option == "5.4.3":
        exo_5_4_3()
    elif option == "5.4.4":
        exo_5_4_4()
    elif option == "5.4.5":
        exo_5_4_5()
    elif option == "5.4.6":
        exo_5_4_6()
    elif option == "5.4.7":
        exo_5_4_7()
    elif option == "5.4.8":
        exo_5_4_8()
    elif option == "5.4.9":
        exo_5_4_9()
    elif option == "5.4.10":
        exo_5_4_10()
    elif option == "5.4.11":
        exo_5_4_11()
    elif option == "5.4.12":
        exo_5_4_12()
    elif option == "5.4.13":
        exo_5_4_13()
    elif option == "5.4.14":
        exo_5_4_14()
    elif option == "6.7.1":
        exo_6_7_1()
    elif option == "6.7.2":
        exo_6_7_2()
    elif option == "6.7.3":
        exo_6_7_3()
    elif option == "6.7.4":
        exo_6_7_4()
    elif option == "6.7.5":
        exo_6_7_5()
    elif option == "6.7.6":
        exo_6_7_6()
    elif option == "6.7.7":
        exo_6_7_7()
    elif option == "6.7.8":
        exo_6_7_8()
    elif option == "6.7.9":
        option_methode = input("Quel méthode voulez vous tester ? (1/2/3) ")
        if option_methode == "1":
            exo_6_7_9_1()
        elif option_methode == "2":
            exo_6_7_9_2()
        elif option_methode == "3":
            exo_6_7_9_3()
    elif option == "6.7.10":
        exo_6_7_10()
    else:
        print("Option inconnue")
    input("\nTapez Entrer pour continuer...")
