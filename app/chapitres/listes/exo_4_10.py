def exo_4_10_1():
    print("4.10.1 Jours de la semaine")

    week = ["Monday", "Tuesday", "Wednesday", "Thirsday", "Friday", "Saturday",
            "Sunday"]

    print("\nQuestion 1 :\n")
    print("5 premiers jours de la semaine : ", week[0:5])
    print("Jours du week-end : ", week[5:])

    print("\nQuestion 2 :\n")
    print("5 premiers jours de la semaine (autre methode) : ", week[0:-2])
    print("Jours du week-end (autre methode) : ", week[5:])

    print("\nQuestion 3 :\n")
    print("Dernier jour de la semaine : ", week[6])
    print("Dernier jour de la semaine (autre methode): ", week[-1])

    print("\nQuestion 4 :\n")
    week = week[::-1]

    print("Jour de la semaine inversé : ", week)


def exo_4_10_2():
    print("4.10.2 Saisons")

    printemps = ["Mars", "Avril", "Mai"]
    ete = ["Juin", "Juillet", "Août"]
    automne = ["Septembre", "Octobre", "Novembre"]
    hiver = ["Décembre", "Janvier", "Février"]
    saisons = [hiver, printemps, ete, automne]

    print("\nsaisons : [")
    for i in range(len(saisons), 1):
        print("\n", saisons[i], end=";")
    print("]")
    print("\n\n1. saisons[2] : ", saisons[2])
    print("2. saisons[1][0] : ", saisons[1][0])
    print("3. saisons[1:2] : ", saisons[1:2])
    print("4. saisons[:][1] : ", saisons[:][1])


def exo_4_10_3():
    print("4.10.3 Table de multiplication par 9")

    print(f"Multiplication par 9 en une seule commande : \
{list(range(0, 91, 9))}")


def exo_4_10_4():
    print("4.10.4 Nombres pairs")

    print(f"\nNombre de nombre paire en une seule commande : \
{len(range(2, 10001, 2))}")


def exo_4_10_5():
    print("4.10.5 List et indice")

    week = ["Monday", "Tuesday", "Wednesday", "Thirsday", "Friday",
            "Saturday", "Sunday"]

    print("week : ", week)
    print("week[4] : ", week[4])

    temp = week[0]
    week[0] = week[-1]
    week[-1] = temp
    print('-'*20+"Apres permutation de week[0] et week[6]"+'-'*12+"\nweek[6] \
* 20 : ", week[6]*12)


def exo_4_10_6():
    print("4.10.6 List et range")

    lstVide = []
    print(f"\nList vide : {lstVide}")
    lsFlottant = [0.0]*5
    print(f"List flottante : {lsFlottant}")
    print('-'*50)
    print(f"Range(4) = {list(range(4))}")
    print(f"Range(4, 8) = {list(range(4, 8))}")
    print(f"Range(2, 9, 2) = {list(range(4, 9, 2))}")
    print('-'*50)

    lstElmnt = list(range(6)) + lstVide + lsFlottant
    print(lstElmnt)
