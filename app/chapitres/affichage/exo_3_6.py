def exo_3_6_1():
    print("3.6.1 Affichage dans l’interpréteur et dans un programme\n")
    print("Dans l'interpreteur python lorsque je tape 1+1, en appyant sur \
entrer l'interpreteur affiche \n2 tandis que si j'ecris la meme chose sur un \
script test.py dans un editeur, le resultat ne \ns'affiche pas. Pour afficher \
le resultat on utilise la methode print donc print(1+1) qui renvoie {1+1}")


def exo_3_6_2():
    print("3.6.2 Poly-A\n")
    print("A" * 20)


def exo_3_6_3():
    print("3.6.3 Poly-GC\n")
    print("A" * 20 + "GC" * 40)


def exo_3_6_4():
    print("3.6.4 Ecriture formatée\n")
    a = "salut"
    b = 102
    c = 10.318

    print(f"{a} {b} {c:.2f}")


def exo_3_6_5():
    print("3.6.5 Ecriture formatée 2\n")
    perc_GC = ((4500 + 2575)/14800)*100

    print(f"Le pourcentage de GC est {perc_GC:.0f} %")
    print(f"Le pourcentage de GC est {perc_GC:.1f} %")
    print(f"Le pourcentage de GC est {perc_GC:.2f} %")
    print(f"Le pourcentage de GC est {perc_GC:.3f} %")


if __name__ == "__main__":
    exo_3_6_1()
