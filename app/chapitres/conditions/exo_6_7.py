def exo_6_7_1():
    """
    Parcours une liste semaine et affiche des messages selon le jour où l'on
    est.
    """
    print("6.7.1 Jours de la semaine\n")

    semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi",
               "dimanche"]

    for jour in semaine:
        if jour in semaine[:4]:
            print("Au travail")
        elif jour in semaine[4]:
            print("Chouette c'est vendredi")
        else:
            print("Repos ce weekend")


def exo_6_7_2():
    """
    Transforme une séquence d'un brin d'ADN en sa séquence complémentaire.
    """
    print("6.7.2 Séquence complémentaire d’un brin d’ADN\n")

    sequence = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C",
                "G"]
    sequence_comp = []

    for e in sequence:
        if e == "A":
            sequence_comp.append("T")
        elif e == "T":
            sequence_comp.append("A")
        elif e == "C":
            sequence_comp.append("G")
        elif e == "G":
            sequence_comp.append("C")

    print(f"{sequence}\n{sequence_comp}")


def exo_6_7_3():
    """
    Affiche l'élément le plus petit d'une liste donnée sans utiliser
    la fonction min().
    """
    print("6.7.3 Minimum d’une liste\n")

    liste = [8, 4, 6, 1, 5]

    min = liste[0]

    for e in liste:
        if min > e:
            min = e
    print(f"Le minimum est : {min}")


def exo_6_7_4():
    print("6.7.4 Fréquence des acides aminés\n")

    sequence = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R",
                "A", "G"]

    ala, arg, tryp, gly = 0, 0, 0, 0

    for e in sequence:
        if e == "A":
            ala += 1
        elif e == "R":
            arg += 1
        elif e == "W":
            tryp += 1
        else:
            gly += 1

    print(f"La fréquence d'alanine est : {(ala/len(sequence))*100:.2f} %\
\nLa fréquence d'arginine est : {(arg/len(sequence))*100:.2f} %\
\nLa fréquence de tryptophane est : {(tryp/len(sequence))*100:.2f} %\
\nLa fréquence de glycine est : {(ala/len(sequence))*100:.2f} %")


def exo_6_7_5():
    print("6.7.5 Notes et mention d’un étudiant\n")

    notes = [14, 9, 13, 15, 12]

    min = notes[0]
    max = notes[0]
    moyenne = 0

    for e in notes:
        if min > e:
            min = e
        if max < e:
            max = e
        moyenne += e

    moyenne /= len(notes)

    print(f"La note minimum est : {min}\nLa note maximum est : {max}\
\nLa moyenne est : {moyenne:.2f}")

    mention = ["n'a pas la moyenne", "passable", "assez-bien", "bien"]

    if moyenne < 10:
        print(mention[0])
    elif 10 <= moyenne < 12:
        print(mention[1])
    elif 12 <= moyenne < 14:
        print(mention[2])
    else:
        print(mention[3])


def exo_6_7_6():
    print("6.7.6 Nombres pairs\n")

    inf_ten = "Les nombres pairs inférieurs à 10 sont : "
    sup_ten = "Les nombres impairs supérieurs à 10 sont : "

    for i in range(21):
        if i < 10 and i % 2 == 0:
            inf_ten += str(i) + " "
        elif i > 10 and i % 2 != 0:
            sup_ten += str(i) + " "

    print(inf_ten)
    print(sup_ten)


def exo_6_7_7():
    print("6.7.7 Conjecture de Syracuse (exercice +++)\n")

    reponse = ""
    while not reponse.isdigit():
        reponse = input("Entrez un nombre (entier positif): ")

    n = int(reponse)

    liste = []

    i = 0
    while i < 50:
        if n % 2 == 0:
            n /= 2
        else:
            n *= 3
            n += 1
        liste.append(n)
        print(int(n), end=" ")
        i += 1


def exo_6_7_8():
    print("6.7.8 Attribution de la structure secondaire des acides aminés \
d’une protéine (exercice +++)\n")

    itfe = [[48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
            [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
            [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
            [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
            [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]]

    for acide_amine in itfe:
        if -57 - 30 <= acide_amine[0] <= -57 + 30 \
          and -47 - 30 <= acide_amine[1] <= -47 + 30:
            print(f"{acide_amine} est en hélice")
        else:
            print(f"{acide_amine} n'est pas en hélice")


def exo_6_7_9_1():
    print("6.7.9 Détermination des nombres premiers inférieurs à 100 \
(exercice +++)\n")
    print("Méthode 1")
    premiers = []

    i = 0
    isPremier = False

    for nombre in range(2, 101):
        for e in range(1, nombre):
            if e != 1 and nombre % e == 0:
                isPremier = False
                break
            else:
                isPremier = True
            i += 1

        if isPremier:
            premiers.append(nombre)
    print(i)

    print("\n"+str(premiers))


def exo_6_7_9_2():
    print("6.7.9 Détermination des nombres premiers inférieurs à 100 \
(exercice +++)\n")
    print("Méthode 2")
    premiers = [2]

    i = 0
    for nombre in range(3, 101):
        isPremier = False
        for e in premiers:
            if nombre % e != 0:
                isPremier = True
            else:
                isPremier = False
                break
            i += 1

        if isPremier:
            premiers.append(nombre)
    print(i)

    print("\n"+str(premiers))


def exo_6_7_9_3():
    print("6.7.9 Détermination des nombres premiers inférieurs à 100 \
(exercice +++)\n")
    print("Méthode 3")

    premiers = []
    non_premiers = []
    nombre = 2

    while nombre <= 100:
        premiers.append(nombre)
        mult = 2
        while nombre * mult <= 100:
            non_premiers.append(nombre * mult)
            mult += 1
        nombre += 1
        while nombre in non_premiers and nombre <= 100:
            nombre += 1

    print(premiers)


def exo_6_7_10():
    print("6.7.10 Recherche d’un nombre par dichotomie (exercice +++)\n")
    print("Pensez à un nombre entre 1 et 100.")

    is_true = False
    nbQuest = 0
    intervalle = [0, 100]
    numQuest = 50

    while not is_true:
        sup_inf = input(f"Est-ce que votre nombre est plus grand, plus petit \
ou égal à {numQuest} ? [+/-/=] ")
        if sup_inf == "=":
            nbQuest += 1
            print(f"Le nombre à trouver est {numQuest}")
            print(f"J'ai trouvé en {nbQuest} questions.")
            is_true = True
        elif sup_inf == "-":
            intervalle[1] = numQuest
            numQuest -= (intervalle[1] - intervalle[0]) / 2
            numQuest = int(numQuest)
            nbQuest += 1
        elif sup_inf == "+":
            intervalle[0] = numQuest
            numQuest += (intervalle[1] - intervalle[0]) / 2
            numQuest = int(numQuest)
            nbQuest += 1
