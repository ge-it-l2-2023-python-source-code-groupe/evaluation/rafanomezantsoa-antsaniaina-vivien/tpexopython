import random


def exo_5_4_1():
    print("5.4.1 Boucles de base")

    list = ["vache", "souris", "levure", "bacterie"]

    print("----------------------------")
    for item in list:
        print(item)

    print("----------------------------")
    for i in range(len(list)):
        print(list[i])

    print("----------------------------")
    i = 0
    while i < len(list):
        print(list[i])
        i += 1
    print("----------------------------")


def exo_5_4_2():
    print("5.4.2 Boucle et jours de la semaine")

    semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi",
               "dimanche"]

    for jour in semaine:
        print(jour)

    i = 5
    while i < len(semaine):
        print(semaine[i])
        i += 1


def exo_5_4_3():
    print("5.4.3 Nombres de 1 à 10 sur une ligne")

    for nombre in range(1, 11):
        print(f"{nombre} ", end=" ")

    print("")


def exo_5_4_4():
    print("5.4.4 Nombres pairs et impairs")

    impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]

    pairs = list()

    for nombre in impairs:
        pairs.append(nombre + 1)

    print(pairs)


def exo_5_4_5():
    print("5.4.5 Calcul de la moyenne")

    notes = [14, 9, 6, 8, 12]
    moyenne = 0

    for note in notes:
        moyenne += note

    moyenne /= len(notes)

    print(f"Moyenne : {moyenne:.2f}")


def exo_5_4_6():
    print("5.4.6 Produit de nombres consécutifs")

    entiers = list(range(2, 21, 2))

    print(entiers)

    for i in range(len(entiers) - 1):
        print(entiers[i] * entiers[i+1])


def exo_5_4_7():
    print("5.4.7 Triangle")

    reponse = ""
    while not reponse.isdigit():
        reponse = input("Entrez un nombre de lignes (entier positif): ")

    N = int(reponse)

    for i in range(N+1):
        print("*" * i)


def exo_5_4_8():
    print("5.4.8 Triangle inversé")

    reponse = ""
    while not reponse.isdigit():
        reponse = input("Entrez un nombre de lignes (entier positif): ")

    N = int(reponse)

    for i in range(N+1):
        print("*" * (N - i))


def exo_5_4_9():
    print("5.4.9 Triangle gauche")

    reponse = ""
    while not reponse.isdigit():
        reponse = input("Entrez un nombre de lignes (entier positif): ")

    N = int(reponse)

    for i in range(N+1):
        print(" " * (N - i) + "*" * i)


def exo_5_4_10():
    print("5.4.10 Pyramide")

    reponse = ""
    while not reponse.isdigit():
        reponse = input("Entrez un nombre de lignes (entier positif): ")

    N = int(reponse)

    for i in range(N+1):
        print(" " * (N - i) + "*" * (2 * i - 1))


def exo_5_4_11():
    print("5.4.11 Parcours de matrice")

    n = 3

    print("ligne  colonne")
    for i in range(n):
        for j in range(n):
            print(f"{i+1:>4d}   {j+1:>4d}")


def exo_5_4_12():
    print("5.4.12 Parcours de demi-matrice sans la diagonale (exercice ++)")

    n = 4

    count = 0

    print("ligne  colonne")
    for i in range(n):
        for j in range(n):
            if j > i:
                print(f"{i+1:>4d}    {j+1:>4d}")
                count += 1

    print(f"Pour une matrice {n}x{n}, on a parcouru {count} cases")


def exo_5_4_13():
    print("5.4.13 Saut de puce")

    i = 0

    while i < 5:
        print(i)
        i += random.choice([-1, 1])


def exo_5_4_14():
    print("5.4.14 Suite de Fibonacci (exercice +++)")

    fibo = [0, 1]

    n = 15

    for i in range(2, n):
        fibo.append(fibo[i-2] + fibo[i-1])
        print(f"x({i+1}) : {fibo[i]:>4} rapport : {fibo[i] - fibo[i-1]:>4d}")
