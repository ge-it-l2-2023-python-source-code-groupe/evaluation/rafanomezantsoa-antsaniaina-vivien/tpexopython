def exo_2_11_1():
    print("2.11.1 Nombres de Friedman\n")

    print(f"7 + 3\u2076 = {7 + 3**6} \u21D2 Le nombre 736 est un nombre de\
Friedman")

    print(f"(3 + 4)\u00B3 = {(3 + 4)**3} \u21D2 Le nombre 343 est un nombre de\
Friedman")

    print(f"3\u2076 − 5 = {3**6 - 5} \u21D2 Le nombre 365 n'est pas un nombre \
de Friedman")

    print(f"(1 + 2\u2078) × 5 = {(1 + 2**8) * 5} \u21D2 Le nombre 1285 est un\
nombre de Friedman")

    print(f"(2 +1\u2078)\u2077 = {(2 + 1**8)**7} \u21D2 Le nombre 2187 est un\
nombre de Friedman")


def exo_2_11_2():
    print("2.11.2 Opérations\n")

    print(f"Prediction des resultats :\
\n>>> (1+2)**3 \n{(1+2)**3}\
\n>>> 'Da' * 4 \n{'Da' * 4}\
\n>>> Da + 3  \nTypeError: can only concatenate str (not int) to str\
\n>>> ('Pa' + 'Pa') * 4 \n{('Pa'+'La') * 2}\
\n>>> ('Da' * 4) / 2 \nTypeError: unsupported operand type(s) for /: 'str' and\
'int'\
\n>>> 5/2 \n{5/2}\
\n>>> 5//2 \n{5//2}\
\n>>> 5%2 \n{5%2}")


def exo_2_11_3():
    print("2.11.3 Opérations et conversion de types\n")

    print(f"Prediction des resultats :\
\n>>> str(4) * int('3') \n{str(4) * int('3')}\
\n>>> int('3') + float('3.2') \n{int('3') + float('3.2')}\
\n>>> str(3) * float('3.2') \nTypeError: can't multiply sequence by non-int of\
 type 'float'\
\n>>> str(3/4) * 2 \n{str(3/4) * 2}")


if __name__ == "__main__":
    exo_2_11_2()
    exo_2_11_3()
