def parse_space(string_args):
    parsed_string = ""
    i = 0
    while i < len(string_args):
        if string_args[i] != ' ':
            parsed_string += string_args[i]
        i += 1
    return parsed_string